{
  description = "papojari's NixOS configuration";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-21.11";
    nixos-hardware.url = github:NixOS/nixos-hardware/master;
    home-manager = {
      url = github:nix-community/home-manager/master;
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nix-doom-emacs.url = "github:vlaci/nix-doom-emacs";
  };

  outputs = { nixpkgs, home-manager, nixos-hardware, nix-doom-emacs, ... }:
  let
    pkgs = import nixpkgs {
      config = {
        # Allow unfree packages (sorry stallman)
        allowUnfree = true;
      };
    };
    lib = nixpkgs.lib;
  in {
    nixosConfigurations = {
      Cryogonal = lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          ./flake-default-modules.nix
          ./hardware/amd.nix
          ./system/amd.nix
          ./.secrets/system/amd.nix
          ./network/amd.nix
          ./packages/extra.nix
          #./programs/openrgb.nix
          ./programs/obs-studio.nix
          ./programs/gaming.nix
          ./programs/printing-scanning/default.nix
          ./programs/printing-scanning/hplip.nix
        ];
      };
      Cryogonull = lib.nixosSystem {
        system = "aarch64-linux";
        modules = [
          ./flake-default-modules.nix
          nixos-hardware.nixosModules.raspberry-pi-4
          ./hardware/rpi4.nix
          ./system/rpi4.nix
          ./network/rpi4.nix
        ];
      };
    };
  };
}
